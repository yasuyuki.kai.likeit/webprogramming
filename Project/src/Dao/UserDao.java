package Dao;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;


public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql = "select * from user where login_id = ? and password = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			String pass = Encryption(password);
			pStmt.setString(2, pass);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(idData, loginIdData, nameData);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;

	}

	public User findByIdinfo(String id) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql = "select * from user where id = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			int idData = rs.getInt("id");
			String loginIdData  = rs.getString("login_id");
			String passwordData  = rs.getString("password");
			Date birthDateData  = rs.getDate("birth_date");
			String nameData  = rs.getString("name");
			String createDateData  = rs.getString("create_date");
			String updateDateData  = rs.getString("update_date");
			return new User(idData, loginIdData, passwordData, birthDateData, nameData, createDateData, updateDateData);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;


	}

	public List<User> findAll(){
		Connection con = null;
		List<User> userList = new ArrayList<User>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from user";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id =rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				Date birthDate = rs.getDate("birth_date");
				String name = rs.getString("name");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				User user = new User(id, loginId, password, birthDate, name, createDate, updateDate);

				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}

	public void insert(String loginId, String password, String birthdate, String name) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();
			String insertSql = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date) VALUES(?,?,?,?,NOW(),NOW())";
			pStmt = con.prepareStatement(insertSql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthdate);
			String pass = Encryption(password);
			pStmt.setString(4, pass);


			pStmt.executeUpdate();
			System.out.println(pStmt);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public User findByLoinIdInfo(String loginId) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql = "select * from user where login_id = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			return new User(loginIdData);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;

	}

	public void update(String id,  String name, String birthDate) {
		Connection con = null;
		PreparedStatement pStmt = null;


		try {
			con = DBManager.getConnection();
			String updataSql = "UPDATE user SET name = ?, birth_date = ?, update_date = NOW() WHERE id = ?";

			pStmt = con.prepareStatement(updataSql);

			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);

			pStmt.executeUpdate();
			System.out.println(pStmt);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void update(String id,  String name, String birthdate, String password) {
		Connection con = null;
		PreparedStatement pStmt = null;


		try {
			con = DBManager.getConnection();
			String updataSql = "UPDATE user SET name = ?,birth_date = ?, password = ?, update_date = NOW() WHERE id = ?";

			pStmt = con.prepareStatement(updataSql);

			pStmt.setString(1, name);
			pStmt.setString(2, birthdate);
			String pass = Encryption(password);
			pStmt.setString(3, pass);
			pStmt.setString(4, id);

			pStmt.executeUpdate();
			System.out.println(pStmt);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void delete(String id) {
		Connection con = null;
		PreparedStatement pStmt = null;


		try {
			con = DBManager.getConnection();
			String deleteSql = "DELETE FROM user WHERE id = ?";

			pStmt = con.prepareStatement(deleteSql);

			pStmt.setString(1, id);

			pStmt.executeUpdate();
			System.out.println(pStmt);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public User UserSearch(String loginId, String name, String birthDate1, String birthDate2) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			if(!(loginId.equals(""))){
				sql += " AND login_id = '" + loginId + "'";
			}

			if(!(name.equals(""))){
				sql += " AND name LIKE '%"+ name +"%'";
			}

			if (!(birthDate1.equals(""))) {
				sql += " AND birth_date >= '" + birthDate1 + "'";
			}

			if (!(birthDate2.equals(""))) {
				sql += " AND birth_date <= '" + birthDate2 + "'";
			}
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			if(!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");
			return new User(idData, loginIdData, nameData, birthDateData);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;

	}

	public String Encryption(String password) {
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		//標準出力
		System.out.println(result);
		return result;

	}

}
