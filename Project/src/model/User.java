package model;

import java.util.Date;

public class User {
	private int id;
	private String loginId;
	private String password;
	private Date birthDate;
	private String name;
	private String createDate;
	private String updateDate;

	public User(int id, String loginId, String name) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
	}

	public User(int id, String loginId, String password, Date birthDate, String name, String createDate, String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.birthDate = birthDate;
		this.name = name;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}



	public User(int id) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
	}

	public User(String loginId) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.loginId = loginId;
	}

	public User(int id, String loginId, String name, Date birthDate) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}
