package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class CreateUserServlet
 */
@WebServlet("/CreateUserServlet")
public class CreateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createUser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String loginid = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		String createdate = request.getParameter("createDate");
		String updateDate = request.getParameter("updateDate");
		if(!(password.equals(repassword))) {
			request.setAttribute("errMsg", "入力された内容が正しくありませんA");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createUser.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(loginid.equals("") || name.equals("") || birthdate.equals("") || password.equals("") || repassword.equals("")) {
			request.setAttribute("errMsg", "入力された内容が正しくありませんB");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createUser.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDao userDao = new UserDao();
		User login_id = userDao.findByLoinIdInfo(loginid);
		if(!(login_id == null)) {
			request.setAttribute("errMsg", "入力された内容が正しくありませんC");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createUser.jsp");
			dispatcher.forward(request, response);
			return;
		}
		userDao.insert(loginid, password, birthdate, name);
		response.sendRedirect("UserListServlet");
	}

}
