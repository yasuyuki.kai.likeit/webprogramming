package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdata
 */
@WebServlet("/UserUpdataServlet")
public class UserUpdataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		User user = userDao.findByIdinfo(id);
		request.setAttribute("user", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdata.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		if(!(password.equals(repassword))) {
			request.setAttribute("errMsg", "入力された内容が正しくありませんA");
			UserDao userDao = new UserDao();
			User user = userDao.findByIdinfo(id);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdata.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(name.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容が正しくありませんB");
			UserDao userDao = new UserDao();
			User user = userDao.findByIdinfo(id);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdata.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDao userDao = new UserDao();

		if (password.equals("") && repassword.equals("")) {
			userDao.update(id, name, birthDate);
		}
		if(!(name.equals("")) && !(birthDate.equals("")) && !(password.equals("")) && !(repassword.equals(""))) {
			userDao.update(id, name, birthDate, password);
		}
		response.sendRedirect("UserListServlet");
	}

}
