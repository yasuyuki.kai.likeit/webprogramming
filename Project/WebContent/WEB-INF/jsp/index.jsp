<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
            .container{
                width: 600px;
                margin-bottom: 20px;
/*                text-align: center;*/
            }
            .heading{
                height: 25px;
            }
        </style>
</head>
<body>
	<div class="container text-center my-3">
            <div class="login-head py-4">
                <h2>ログイン画面</h2>
            </div>
            <div class="container text-center">
				<c:if test="${ errMsg != null}">
				<h3 class="alert alert-light text-danger" role="alert">
  					${errMsg}
				</h3>
				</c:if>
			</div>
            <form action="LoginServlet" method="post">
                <div class="login-id pb-4">
                    <label class="pr-4" for="id">ログインID</label>
                    <input class="rounded-sm" type="text" id="loginid" name="loginid">
                </div>
                <div class="password pb-4">
                    <label class="pr-4" for="password">パスワード</label>
                    <input class="rounded-sm" tyep="password" id="password" name="password">
                </div>
                <div class="login-btn">
                    <input class="px-4" type="submit" value="ログイン">
                </div>
            </form>
     </div>
</body>
</html>