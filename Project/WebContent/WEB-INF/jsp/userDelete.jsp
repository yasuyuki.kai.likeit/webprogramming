<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">
<head>
<meta chraset="utf-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<style>
.container {
	width: 600px;
	margin-bottom: 20px;
	/*        text-align: center;        */

}

.heading {
	height: 25px;
}
</style>
</head>
<body>
	<header>
		<div class="container">
			<div class="heading bg-secondary">
				<a class="text-danger float-right" href="LoginServlet">ログアウト</a>
				<p class="mr-5 float-right user-name text-white">
					${userInfo.name} さん</p>
			</div>
		</div>
	</header>
	<div class="title">
		<div class="container text-center">
			<h2>ユーザー削除確認</h2>
		</div>
	</div>
	<div class="container">
		<p>
			ログインID:${user.loginId}<br>を本当に削除してよろしいでしょうか。
		</p>
		<div class="row text-center mt-5">
			<form action="UserListServlet" method="get">
				<div class="col-md-7">
					<input type="submit" value="キャンセル"class="px-4 mx-5">
				</div>
			</form>
			<form action="UserDeleteServlet" method="post">
				<input name="id" value="${user.id}" type="hidden">
				<div class="col-md-7">
					<input type="submit" value="OK" class="px-5 ml-5">
				</div>
			</form>


		</div>
	</div>
</body>
</html>