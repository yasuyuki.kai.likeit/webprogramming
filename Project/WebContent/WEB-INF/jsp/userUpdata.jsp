<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">
    <head>
        <meta chraset="utf-8">
        <title>ユーザー新規登録</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
            .container{
                width: 600px;
                margin-bottom: 20px;
/*                text-align: center;*/
            }
            .heading{
                height: 25px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="heading bg-secondary">
                    <a class="text-danger float-right" href="LoginServlet">ログアウト</a>
                    <p class="mr-5 float-right user-name text-white">
                    ${userInfo.name} さん
                    </p>
                </div>
            </div>
        </header>
        <div class="title">
            <div class="container text-center">
                <h2>ユーザー情報更新</h2>
            <c:if test="${ errMsg != null}">
					<h3 class="alert alert-light text-danger" role="alert">
  						${errMsg}
					</h3>
			</c:if>
			</div>
        </div>
        <div class="form">
        <form action="UserUpdataServlet" method="post">
            <div class="container">
                <div class="row mb-4">
                <input name = "id" value="${user.id}" type="hidden">
                    <div class="col-md-6">
                        <h5>ログインID</h5>
                    </div>
                    <div class="col-md-6">
                        <input type="text" style="width:200px;" class="form-control" value="${user.loginId}" readonly>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>ユーザー名</h5>
                    </div>
                    <div class="col-md-6">
                        <input type="text" style="width:200px;" class="form-control" value="${user.name}" name="name">
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>生年月日</h5>
                    </div>
                    <div class="col-md-6">
                        <input type="text" style="width:200px;" class="form-control" value="${user.birthDate}" name="birthDate">
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>パスワード</h5>
                    </div>
                    <div class="col-md-6">
                        <input type="password" style="width:200px;" class="form-control" name="password">
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>パスワード(確認)</h5>
                    </div>
                    <div class="col-md-6">
                        <input type="password" style="width:200px;" class="form-control" name="repassword">
                    </div>
                </div>
                <div class="update_date">
                  	<input type="hidden" value="<%= System.currentTimeMillis() %>">
                </div>
                <div class="text-center">
                        <input type="submit" value="更新" class="px-5">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="UserListServlet">戻る</a>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </body>
</html>