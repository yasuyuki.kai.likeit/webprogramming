<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta chraset="utf-8">
        <title>ユーザー情報詳細参照</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
            .container{
                width: 600px;
                margin-bottom: 20px;
/*                text-align: center;*/
            }
            .heading{
                height: 25px;
            }
        </style>
</head>
<body>
	<header>
            <div class="container">
                <div class="heading bg-secondary">
                    <a class="text-danger float-right" href="LoginServlet">ログアウト</a>
                    <p class="mr-5 float-right user-name text-white">
                    		${userInfo.name}さん
                    </p>
                </div>
            </div>
        </header>
        <div class="title">
            <div class="container text-center">
                <h2>ユーザー情報詳細参照</h2>
            </div>
        </div>
        <div class="form">
            <div class="container">

                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>ログインID</h5>
                    </div>
                    <div class="col-md-6">
                        <p>${user.loginId}</p>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>名前</h5>
                    </div>
                    <div class="col-md-6">
                        <p>${user.name}</p>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>生年月日</h5>
                    </div>
                    <div class="col-md-6">
                        <p>${user.birthDate}</p>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>登録日時</h5>
                    </div>
                    <div class="col-md-6">
                        <p>${user.createDate}</p>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h5>更新日時</h5>
                    </div>
                    <div class="col-md-6">
                        <p>${user.updateDate}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="UserListServlet">戻る</a>
                    </div>
                </div>

            </div>
        </div>
</body>
</html>