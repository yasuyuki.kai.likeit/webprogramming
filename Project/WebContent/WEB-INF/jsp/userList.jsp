<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta chraset="utf-8">
        <title>ユーザー一覧</title>
<!--        <link href="css/login.css" rel="stylesheet">-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            .container{
                width: 900px;
                margin-bottom: 20px;
            }
            .heading{
                height: 25px;
            }
        </style>
</head>
<body>
	<header>
            <div class="container">
                <div class="heading bg-secondary">
                	<form action="LogOutServlet" method="post">
                		<a class="text-danger float-right" href="LoginServlet">ログアウト</a>
                	</form>
                    <p class="mr-5 float-right user-name text-white">
                   		${userInfo.name}さん
                    </p>
                </div>
            </div>
        </header>
        <div class="title">
            <div class="container text-center">
                <h2>ユーザー一覧</h2>
            </div>
        </div>
        <div class="sign-up">
            <div class="container my-4 text-right">
            		<a href="CreateUserServlet">新規登録</a>
            </div>
        </div>
        <div class="search">
            <div class="container">
              <form action="UserListServlet" method="post">
                <div class="row mb-4">
                    <div class="col-md-4">
                        <h5>ログインID</h5>
                    </div>
                    <div class="col-md-6">
                        <input name="loginId" type="text" style="width:350px" class="form-control">
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-md-4">
                        <h5>ユーザー名</h5>
                    </div>
                    <div class="col-md-6">
                        <input name="name" type="text" style="width:350px;" class="form-control">
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-4">
                        <h5>生年月日</h5>
                    </div>
                    <div class="col-md-6">
                        <p><input type="date" name="birthDate1">～　<input type="date" name="birthDate2"></p>
                    </div>
                </div>
                <div class="text-right pb-3 border-bottom">
                        <input type="submit" value="検索" class="px-5">
                </div>
			  </form>
            </div>
        </div>
        <div class="users">
            <div class="container">
                <table class="table-bordered" width="900px">
                    <tr>
                        <th>ログインID</th>
                        <th>ユーザー名</th>
                        <th>生年月日 </th>
                        <th></th>
                    </tr>

                    <c:forEach var="user" items="${userList}">
                    	<c:if test="${userInfo.id != 24 && user.id  != 24 && search == null}">
                    		<tr>
                     			<td>${user.loginId}</td>
                     			<td>${user.name}</td>
                     			<td>${user.birthDate}</td>
                     			<td>
                     				<a href="UserDetailServlet?id=${user.id}" class="btn btn-primary px-4 mx-3">詳細</a>
                     				<c:if test="${userInfo.id == user.id || userInfo.id == 24}">
                           				<a href="UserUpdataServlet?id=${user.id}" class="btn btn-success px-4 mx-3">更新</a>
                            		</c:if>
                     			</td>
                     		</tr>
                     	</c:if>

                    <c:if test="${userInfo.id == 24 && search.id == null}">
                    	<tr>
                     		<td>${user.loginId}</td>
                     		<td>${user.name}</td>
                     		<td>${user.birthDate}</td>
                     		<td>
                     			<a href="UserDetailServlet?id=${user.id}" class="btn btn-primary px-4 mx-3">詳細</a>
                           		<a href="UserUpdataServlet?id=${user.id}" class="btn btn-success px-4 mx-3">更新</a>
                       			<a href="UserDeleteServlet?id=${user.id}" class="btn btn-danger px-4 mx-3">削除</a>
                     		</td>
                    	</tr>
                     </c:if>
					</c:forEach>
                     <c:if test="${search.id != null && search.id != 24}">
                     	<tr>
                     		<td>${search.loginId}</td>
                     		<td>${search.name}</td>
                     		<td>${search.birthDate}</td>
                     		<td>
                     			<a href="UserDetailServlet?id=${search.id}" class="btn btn-primary px-4 mx-3">詳細</a>
                     				<c:if test="${userInfo.id == search.id || userInfo.id == 24}">
                           				<a href="UserUpdataServlet?id=${user.id}" class="btn btn-success px-4 mx-3">更新</a>
                            		</c:if>
                            		<c:if test="${userInfo.id == 24}">
                       					<a href="UserDeleteServlet?id=${search.id}" class="btn btn-danger px-4 mx-3">削除</a>
                       				</c:if>
                     		</td>
                    	</tr>
                     </c:if>

                </table>
            </div>

        </div>

</body>
</html>