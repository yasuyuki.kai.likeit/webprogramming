﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

create table user(
id SERIAL,
login_id varchar(255) unique not null,
name varchar(255) unique not null,
birth_date date not null,
password varchar(255) not null,
create_date datetime not null,
update_date datetime not null);


INSERT INTO user VALUES (1, 'admin', '管理者',  '1997-11-11', 'password', NOW(), NOW());
